
function y = sulfateguessIso(x,Sigma,zetaSigma)
         y = [max([eps 0.0431*interp1(zetaSigma,Sigma(:,1),x)])
      min([-eps 0.0431*interp1(zetaSigma,Sigma(:,2),x)])
     max([eps interp1(zetaSigma,Sigma(:,1),x)])
      min([-eps interp1(zetaSigma,Sigma(:,2),x)])];
    end
 
            