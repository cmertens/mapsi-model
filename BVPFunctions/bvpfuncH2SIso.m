function dHdz = bvpfuncH2SIso(z,H,Parameters,Boundaries,BvPSolutionSulfateIso,zetaEta,Eta)
Gamma=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da.*z))).^Parameters.a;
Psi=Boundaries.Psi0*(Parameters.b./(Parameters.b+(Parameters.Da*Parameters.chi*z))).^Parameters.b;
SzetaIso=BvPSolutionSulfateIso.x;
Sol34=BvPSolutionSulfateIso.y(1,:);
dS=interp1(SzetaIso,BvPSolutionSulfateIso.y(2,:),z);
Sigma34=interp1(SzetaIso,Sol34,z);
Sol32=BvPSolutionSulfateIso.y(3,:);
Sigma32=interp1(SzetaIso,Sol32,z);
Htot=max([interp1(zetaEta,Eta,z) eps]);
dHdz =[H(2) %d34H
     1/(Parameters.Delta)*(H(2)/Parameters.Da-Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(Sigma34+Sigma32)/(Parameters.KappaS+Sigma34+Sigma32)*Parameters.alpha*Sigma34/(max([eps Sigma32+Parameters.alpha*Sigma34]))+H(1)/(Parameters.KappaE+Htot)*Psi*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z))                                            
   H(4) %d32H
    1/(Parameters.Delta)*(H(4)/Parameters.Da-Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(Sigma34+Sigma32)/(Parameters.KappaS+Sigma34+Sigma32)*Sigma32/(max([eps Parameters.alpha*Sigma34+Sigma32]))+H(3)/(Parameters.KappaE+Htot)*Psi*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z))];
     end