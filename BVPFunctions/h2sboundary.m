function res = h2sboundary(ya,yb,Eta0)
res = [ya(1)-Eta0 %H2S concentration at beginning=0
    yb(2)];%H2S derivative at depth =0
end