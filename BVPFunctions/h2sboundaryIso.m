function res = h2sboundaryIso(ya,yb,ic0EtaIso32,ic0EtaIso34)
res = [ya(1)-ic0EtaIso34
    yb(2)%derivative at depth =0
    ya(3)-ic0EtaIso32
    yb(4)]; %derivative at depth =0
end

