function y = h2sguess(x,EtaOde,zetaEtaOde)
y = [interp1(zetaEtaOde,EtaOde(:,1),x)
    interp1(zetaEtaOde,EtaOde(:,2),x)];
end
