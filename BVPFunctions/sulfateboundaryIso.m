 function res = sulfateboundaryIso(ya,yb,s0,Rseawater)
        res = [ya(1)-s0*Rseawater/(1+Rseawater)
        yb(2)
        ya(3)-s0/(1+Rseawater)
        yb(4)]; %derivative at depth =0
    end