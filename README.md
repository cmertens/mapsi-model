<!DOCTYPE html>
# MaPSI Model


<html>
<head>
    <title>Marine Pyrite sulfur Isotope Model</title>
</head>
<body>

<h1>Non-Dimensional Geochemical Model</h1>

<p>This repository contains a MATLAB-based non-dimensional model designed to calculate concentrations of various geochemical species and their isotopes. The model focuses on organic carbon (OC) content, sulfate concentration, reduced sulfur (S<sup>-II</sup>) concentration, reactive iron content, pyrite content, and the isotopic compositions of sulfate, S<sup>-II</sup>, and pyrite.</p>

<h2>Repository Structure</h2>

<pre>
Model/
├── BVPFunctions
├── CalculatingConstants
├── Data_Analysis_Functions
├── Data_Extraction_Functions
├── Data_Processing_Functions
├── Global_Data_Validation
├── GlobalMaps
├── HeatMaps
├── ImportProfileDataFunctions
├── NonDimensionalizeFunctions
├── ODE
├── Plotting_Functions
├── SolverFunctions
├── Verification
├── Main.m
├── Validation.m
</pre>

<h3>Key Files and Directories</h3>

<ul>
    <li><strong>Main.m</strong>: The main script for running the model. Use this file to input custom data and execute the model.</li>
    <li><strong>Validation.m</strong>: Script for validating the model against known data.</li>
    <li><strong>codeSetup.pdf</strong>: Detailed documentation on the code structure and setup.</li>
</ul>

<h3>Function Directories</h3>

<ul>
    <li><strong>BVPFunctions</strong>: Functions related to boundary value problems.</li>
    <li><strong>CalculatingConstants</strong>: Functions for calculating constants used in the model.</li>
    <li><strong>Data_Analysis_Functions</strong>: Functions for analyzing input data and deriving relationships.</li>
    <li><strong>Data_Extraction_Functions</strong>: Functions for extracting data from various sources.</li>
    <li><strong>Data_Processing_Functions</strong>: Functions for processing input data.</li>
    <li><strong>Global_Data_Validation</strong>: Functions for validating global datasets.</li>
    <li><strong>GlobalMaps</strong>: Functions for generating global maps of model outputs.</li>
    <li><strong>HeatMaps</strong>: Functions for generating heatmaps of model outputs.</li>
    <li><strong>ImportProfileDataFunctions</strong>: Functions for importing validation site data.</li>
    <li><strong>NonDimensionalizeFunctions</strong>: Functions for non-dimensionalizing the model equations.</li>
    <li><strong>ODE</strong>: Functions for solving ordinary differential equations.</li>
    <li><strong>Plotting_Functions</strong>: Functions for plotting data and results.</li>
    <li><strong>SolverFunctions</strong>: Functions for various numerical solvers.</li>
    <li><strong>Verification</strong>: Functions and scripts for verifying model accuracy.</li>
</ul>

<h2>Getting Started</h2>

<h3>Prerequisites</h3>
<p>MATLAB (version R2021a or later recommended).</p>

<h3>Setup</h3>

<ol>
    <li><strong>Clone the Repository</strong>:
    <pre><code>git clone https://your-gitlab-repo-url.git</code></pre>
    </li>
    <li><strong>Navigate to the Repository Directory</strong>:
    <pre><code>cd Model</code></pre>
    </li>
    <li><strong>Open MATLAB</strong>:
    <p>Launch MATLAB and set the repository directory as the current working directory.</p>
    </li>
</ol>

<h3>Running the Model</h3>
<ol>
    <li>Open <code>Main.m</code> in MATLAB.</li>
    <li>Modify the input parameters as needed to customize the data.</li>
    <li>Run <code>Main.m</code> to execute the model.</li>
</ol>

<h3>Validating the Model</h3>

<ol>
    <li>Open <code>Validation.m</code> in MATLAB.</li>
    <li>Follow the instructions in the script to validate the model against known data.</li>
</ol>

<h3>Global Maps</h3>

<ol>
    <li>Contact me for raw input data grids. Due to their size, only processed data inputs are shared here.</li>
    <li>Open <code>GlobalMaps/Global.m</code> in MATLAB.</li>
    <li>Adjust file paths to your input data grids, or use grids in <code>GlobalMapsInput</code>.</li>
</ol>

<h2>Usage</h2>

<h3>Input Data</h3>
<p>Customize the input data in <code>Main.m</code> according to your requirements. Refer to the comments within the script for guidance on modifying the parameters.</p>

<h3>Output Data</h3>
<p>The model generates output data that can be analyzed and visualized using the functions in the <code>Plotting_Functions</code> directory.</p>

<h2>Contributing</h2>
<p>If you wish to contribute to this project, please follow these steps:</p>

<ol>
    <li>Contact me first to discuss your intended contribution. You can reach me at <a href="mailto:cornelia.mertensl@erdw.ethz.ch">cornelia.mertensl@erdw.ethz.ch</a>.</li>
    <li>Fork the repository.</li>
    <li>Create a new branch (e.g., <code>git checkout -b feature-branch</code>).</li>
    <li>Make your changes.</li>
    <li>Commit your changes (e.g., <code>git commit -am 'Add new feature'</code>).</li>
    <li>Push to the branch (e.g., <code>git push origin feature-branch</code>).</li>
    <li>Create a new Pull Request.</li>
</ol>

<p>Thank you for your interest in contributing!</p>

</body>
</html>
