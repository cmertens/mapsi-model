function Solution=ModelSolverNonDim(NonDimBoundaries,NonDimParameters,zetamesh)
%%Specify shooting function setup
maxIter=50; %iteration of shooting function to find condition at end
ErrTol=10^-6; %error tolerance of solution of shooting function
options = odeset('NonNegative',1);
%% OC
Gamma=NonDimBoundaries.Gamma0*(NonDimParameters.a./(NonDimParameters.a+(NonDimParameters.Da.*zetamesh))).^NonDimParameters.a;

%% Sulfate
ICguesses = [0 -500]; %initial guesses for shooting function
odefun = @(zeta,Sigma) odefuncSulfate(zeta,Sigma,NonDimParameters,NonDimBoundaries);%equations
cEndS=0; %derivative at z=inf;
icS=NonDimBoundaries.Sigma0; %initial condition for concentration
newIC=shooting(ICguesses,odefun,zetamesh,ErrTol,maxIter,[],[],icS,cEndS,options);
%Solve ODE
[zetaSigma,Sigma] = ode45(odefun, zetamesh, [NonDimBoundaries.Sigma0 newIC],options);

%% Reactive Iron
Psi=NonDimBoundaries.Psi0*(NonDimParameters.b./(NonDimParameters.b+(NonDimParameters.Da*NonDimParameters.chi*zetamesh))).^NonDimParameters.b;

%% H2S
%initial guess with ode45
ICguesses = [0 -newIC];
odefun = @(zeta,Eta) odefuncH2S(zeta,Eta,NonDimParameters,NonDimBoundaries,zetaSigma,Sigma);
ic1=NonDimBoundaries.Eta0; %H2S concentration initial condition
icend=0; %derivative of H2S concentration at end =0;
newICH2S=shooting(ICguesses,odefun,zetamesh,ErrTol,maxIter,[],[],ic1,icend,options);
%Solve ODE
[zetaEtaOde,EtaOde] = ode45(odefun, zetamesh, [ic1 newICH2S],options);
% solve with BvP
bvpfunEta = @(zeta,Eta) bvpfuncH2S(zeta,Eta,NonDimParameters,NonDimBoundaries,zetaSigma,Sigma);
bvpbound=@(ya,yb) h2sboundary(ya,yb,NonDimBoundaries.Eta0);%boundary conditions
solinitFunc=@(x)h2sguess(x,EtaOde,zetaEtaOde);%initial guess
solinit = bvpinit(zetamesh,solinitFunc);%initial guess
BvPSolutionH2S= bvp5c(bvpfunEta,bvpbound,solinit);
Eta=BvPSolutionH2S.y(1,:);
zetaEta=BvPSolutionH2S.x;

%% Pyrite
[zPi,Pi] = ode45(@(zPi,Pi) odefuncPi(zPi,NonDimParameters,NonDimBoundaries,zetaEta,Eta), zetamesh, NonDimBoundaries.Pi0);

%% Sulfate Isotopes
%limit depth until depth where sulfate concentration is very small
idx=find(Sigma(:,1)<1e-9,1); %sulfate concentrations below 1e-9
if isempty(idx)
 idx=numel(zetamesh);
end
zetameshBvP=linspace(0,zetamesh(min([idx numel(zetamesh)])),1000);
delta34S0=21.0; %d34S in seawater
gamma=0.044162;%standard isotopic 34S/32S ratio in VCDT
Rseawater=gamma*(delta34S0/1000+1); %34/32S ratio in seawater
odefunIso = @(zeta,SigmaIso) bvpfuncSulfateIso(zeta,SigmaIso,NonDimParameters,NonDimBoundaries);%equations
odeboundIso=@(ya,yb) sulfateboundaryIso(ya,yb,NonDimBoundaries.Sigma0,Rseawater);%boundary conditions
solinitFunc=@(x)sulfateguessIso(x,Sigma,zetaSigma);%initial guess
solinitIso = bvpinit(zetameshBvP,solinitFunc);%initial guess
%solve for SO4 concentration for each isotope
BvPSolutionIso= bvp5c(odefunIso,odeboundIso,solinitIso);
deltaIso=BvPSolutionIso.y(1,:)./(BvPSolutionIso.y(3,:));
delta34Sigma=((deltaIso./gamma)-1)*1000;

%% H2SIsotopes
RseawaterH2S=gamma*(-40/1000+1); %34/32S ratio in seawater
ic0EtaIso34=NonDimBoundaries.Eta0*RseawaterH2S/(1+RseawaterH2S);
ic0EtaIso32=NonDimBoundaries.Eta0/(1+RseawaterH2S);
odefunEtaIso = @(zeta,EtaIso) bvpfuncH2SIso(zeta,EtaIso,NonDimParameters,NonDimBoundaries,BvPSolutionIso,zetaEta,Eta);%equations
odeboundIso=@(ya,yb) h2sboundaryIso(ya,yb,ic0EtaIso32,ic0EtaIso34);%boundary conditions
solinitFunc=@(x)h2sguessIso(x,BvPSolutionH2S.y,zetaEta);%initial guess
solinitIso = bvpinit(zetameshBvP,solinitFunc);%initial guess
%solve for SO4 concentration for each isotope
BvPSolutionH2SIso= bvp5c(odefunEtaIso,odeboundIso,solinitIso);
deltaIsoEta=BvPSolutionH2SIso.y(1,:)./BvPSolutionH2SIso.y(3,:);
delta34Eta=((deltaIsoEta./gamma)-1)*1000;

%% Pyrite Isotopes
options = odeset('NonNegative',1,'MaxStep',1e-3);
%32Pyrite
[~,Pi32] = ode45(@(zPi32,Pi32) odefuncPi32(zPi32,NonDimParameters,NonDimBoundaries,zetaEta,Eta,BvPSolutionH2SIso.x,BvPSolutionH2SIso.y(3,:)),zetamesh, 0,options);
%alternative
%34Pyrite
[~,Pi34] = ode45(@(zPi34,Pi34) odefuncPi34(zPi34,NonDimParameters,NonDimBoundaries,zetaEta,Eta,BvPSolutionH2SIso.x,BvPSolutionH2SIso.y(1,:)),zetamesh, 0,options);
%calculate delta
deltaIsoPi=Pi34(:,1)./Pi32(:,1);
delta34Pi=((deltaIsoPi./gamma)-1)*1000;

%% Build Structure
field1='Gamma';value1={zetamesh, Gamma};
field2='Sigma';value2={zetaSigma,Sigma(:,1)};
field3='Eta';value3={zetaEta,Eta}; %H2S
field4='Psi';value4={zetamesh,Psi}; %Reactive Iron
field5='Pi';value5={zPi,Pi};
field6='d34Sigma';value6={BvPSolutionIso.x,delta34Sigma};
field7='d34Eta';value7={BvPSolutionH2SIso.x,delta34Eta};
field8='d34Pi';value8={zetamesh,delta34Pi};
Solution=struct(field1,value1,field2,value2,field3,value3,field4,value4,field5,value5,field6,value6,field7,value7,field8,value8);
end

