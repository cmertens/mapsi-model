function fF=calcfF(phi,rho)
fF=10^4*2*(1/55.845)*rho*((1- phi)/ phi);
end