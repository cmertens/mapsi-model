function z0=calcz0(w)
%Oxygen penetration depth
OPD=10.^(-0.6864*log10(w)-0.8963);%in cm
%onset of sulphidic zone
z0=OPD*10;
end