function fG=calcfG(porosity,rho)
fG=10^4*0.5882*(1/12)*rho*((1-porosity)/porosity);
end