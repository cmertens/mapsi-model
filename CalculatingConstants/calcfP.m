function fP=calcfP(porosity,rho)
fP=10^4*2*(1/119)*rho*((1-porosity)/porosity);
end