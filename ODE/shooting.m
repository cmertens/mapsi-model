function newIC=shooting(ICguesses,odefun,zetamesh,ErrTol,maxIter,ub,lb,ic,icend,options)

a=ICguesses(:,1);
b=ICguesses(:,2);
%look for sign
c=(a+b)/2;
iter=0;

while abs(sum(shootODE(odefun,zetamesh,ic,c,options,icend)))>ErrTol&&iter<maxIter

    s1=sign(shootODE(odefun,zetamesh,ic,c,options,icend));
    s2=sign(shootODE(odefun,zetamesh,ic,a,options,icend));
%     [xx,yy]=ode45(odefun, zetamesh, [ic c],options);
% plot(yy(:,1),-xx)
%     a
%     b
%     c
    if s1==s2
        a=c;
    else
        b=c;
    end
    c=(a+b)/2;
    iter=iter+1;

%     if iter==100
%         fprintf('Iteration limit reached')
%     end 

end

newIC=c;

    function shoot=shootODE(odefun,zetamesh,ic,ic2,opts,icend)
        [x1,y1] = ode45(odefun, zetamesh, [ic ic2'],opts);
        shoot = y1(end,2)-icend;
    end
end