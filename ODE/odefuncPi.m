function dPidz=odefuncPi(zPi,Parameters,Boundaries,zetaEta,Eta)
Psi=Boundaries.Psi0*(Parameters.b./(Parameters.b+(Parameters.Da*Parameters.chi*zPi))).^Parameters.b;
H=interp1(zetaEta,Eta,zPi);
dPidz=H/(Parameters.KappaE+H)*Parameters.chi*Parameters.Da*Psi*(Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*zPi));
end