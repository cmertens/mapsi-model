function [NonDimData]=makeNonDimData(Data,Parameters,Boundaries)

%OC
zfactor=sqrt(Parameters.kg0/Parameters.DS);
Gamma=(Data(2).G).*Parameters.fG./Boundaries.S0; %Non dimensional OC
zetaGamma=(Data(1).G)*zfactor;%nonDimensional depth OC
%Sulfate
Sigma=(Data(2).S)./Boundaries.S0; %Non-dimensional Sulfate
zetaSigma=(Data(1).S)*zfactor; %non dimensional Depth for sulfate sample points
%H2S
Eta=(Data(2).H)./Boundaries.S0; %Non-dimensional H2S
zetaEta=(Data(1).H)*zfactor; %non dimensional Depth for sulfate sample points
%Iron
Psi=(Data(2).F).*Parameters.fF./Boundaries.S0;  %Non-dimensional Iron
zetaPsi=(Data(1).F)*zfactor; %non dimensional Depth for sulfate sample points
%Pyrite
Pi=(Data(2).P)*Parameters.fP./(Boundaries.S0);  %Non-dimensional Pyrite
zetaPi=(Data(1).P)*zfactor;
%d34S
d34Sigma=(Data(2).d34S); %Non-dimensional Sulfate isotopes
zetad34Sigma=(Data(1).d34S)*zfactor;
%d34H
d34Eta=(Data(2).d34H); %Non-dimensional H2S isotopes
zetad34Eta=(Data(1).d34H)*zfactor;
%d34P
d34Pi=(Data(2).d34P); %Non-dimensional Pyrite isotopes
zetad34Pi=(Data(1).d34P)*zfactor;

%% Build struct with nondim Data
field1='Gamma';value1={zetaGamma, Gamma};
field2='Sigma';value2={zetaSigma, Sigma};
field3='Eta';value3={zetaEta, Eta};
field4='Psi';value4={zetaPsi, Psi};
field5='Pi';value5={zetaPi, Pi};
field6='d34Sigma';value6={zetad34Sigma, d34Sigma};
field7='d34Eta';value7={zetad34Eta, d34Eta};
field8='d34Pi';value8={zetad34Pi, d34Pi};
NonDimData=struct(field1,value1,field2,value2,field3,value3,field4,...
    value4,field5,value5,field6,value6,field7,value7,field8,value8);



