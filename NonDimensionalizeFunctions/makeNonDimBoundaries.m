function [NonDimBoundaries]=makeNonDimBoundaries(Parameters,Boundaries)

%% Build struct with NonDim Boundaries
field1='Gamma0';value1=Boundaries.G0*Parameters.fG/Boundaries.S0;
field2='Sigma0';value2=Boundaries.S0/Boundaries.S0;
field3='Psi0';value3=Boundaries.F0*Parameters.fF/Boundaries.S0;
field4='Pi0';value4=Boundaries.P0*Parameters.fP./(Boundaries.S0);
field5='Eta0';value5=0;

NonDimBoundaries=struct(field1,value1,field2,value2,field3,value3,field4,...
    value4,field5,value5);

