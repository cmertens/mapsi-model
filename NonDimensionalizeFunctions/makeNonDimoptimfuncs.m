function [optimfunc]=makeNonDimoptimfuncs(whichp)
optimplotDa=@(x,optimValues,state) myoptimplot(x,optimValues,state,whichp,'Da');
optimplotDelta=@(x,optimValues,state) myoptimplot(x,optimValues,state,whichp,'Delta');
optimplotChi=@(x,optimValues,state) myoptimplot(x,optimValues,state,whichp,'chi');
optimplotalpha=@(x,optimValues,state) myoptimplot(x,optimValues,state,whichp,'alpha');
optimplotGamma0=@(x,optimValues,state) myoptimplot(x,optimValues,state,whichp,'Gamma0');

alloptimfuncs={optimplotDa optimplotDelta optimplotChi optimplotalpha optimplotGamma0}';
 pTable=table(alloptimfuncs,'RowNames',{'Da';'Delta'; 'chi';'alpha';'Gamma0'});
optimfunc=pTable{whichp,'alloptimfuncs'};
optimfunc{end+1}=@optimplotfval;

end