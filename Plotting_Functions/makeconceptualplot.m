function makeconceptualplot(ModelSolution,color)
figure
nexttile
 plot(ModelSolution(2).Gamma,-ModelSolution(1).Gamma,'LineWidth',1,'Color',color)
 xlabel('\Gamma')
 ylabel('\zeta')
 title('Non-Dimensional OC')
 nexttile
 plot(ModelSolution(2).Sigma,-ModelSolution(1).Sigma,'LineWidth',1,'Color',color)
 xlabel('\Sigma')
 ylabel('\zeta')
  title('Non-Dimensional Sulfate')
 nexttile
 plot(ModelSolution(2).Eta,-ModelSolution(1).Eta,'LineWidth',1,'Color',color)
 xlabel('\eta')
 ylabel('\zeta')
  title('Non-Dimensional H_2S')
 nexttile
 plot(ModelSolution(2).Psi,-ModelSolution(1).Psi,'LineWidth',1,'Color',color)
 xlabel('\Psi')
 ylabel('\zeta')
  title('Non-Dimensional Fe_{HR}')
 nexttile
 plot(ModelSolution(2).Pi,-ModelSolution(1).Pi,'LineWidth',1,'Color',color)
 xlabel('\Pi')
 ylabel('\zeta')
  title('Non-Dimensional Pyrite')
 nexttile
 plot(ModelSolution(2).d34Sigma,-ModelSolution(1).d34Sigma,'LineWidth',1,'Color',color)
 xlabel('\delta^{34}S_{SO_4^{2-}}')
 ylabel('\zeta')
  title('Sulfate Isotopes')
 nexttile
  plot(ModelSolution(2).d34Eta,-ModelSolution(1).d34Eta,'LineWidth',1,'Color',color)
  xlabel('\delta^{34}S_{H_2S}')
 ylabel('\zeta')
  title('H_2S Isotopes')
 nexttile
  plot(ModelSolution(2).d34Pi,-ModelSolution(1).d34Pi,'LineWidth',1,'Color',color)
  xlabel('\delta^{34}S_{FeS_2}')
   title('Pyrite Isotopes')
 ylabel('\zeta')
end