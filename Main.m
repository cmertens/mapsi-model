% Main Routine for Modeling Relevant Species
% This routine models all relevant species for a given set of parameters and boundary conditions.
% User inputs are specified in the "User Inputs" section below.

%% User Inputs: Define Parameters
% Modify the parameters and boundary conditions as needed. These are
% specified as at the water-sediment-interface. We assume
% Organic carbon decays within the non-sulfidic oxygenated part of the
% sediment. You can change the depth we assume (10 x oxygen penetration
% depth)  in the function CalculatingConstants/calcz0. 

% Basic Parameters
rho = 2.7;  % Sediment solid phase density [g cm-3]
phi = 0.5;  % Porosity [fraction]
T = 6.5;    % Bottom water temperature [°C]
w = 0.02;   % Sedimentation rate [cm y-1]
Depth = 100; % Depth until model will solve [cm]

% Constants
a = 0.23;   % Constant for OC decay
b = 0.17;   % Constant for Fe decay
Ks = 1.62;  % Monod coefficient for sulfate
Kh = 1.62;  % Monod coefficient for H2S

% Boundary Conditions
G0 = 3;     % Organic carbon [wt%]
S0 = 28;    % Sulfate [mM]
F0 = 1;     % Reactive Iron [wt%]
P0 = 0;     % Pyrite [wt%]
d34S0 = 21; % delta34S sulfate

%% Processing Inputs
% Organize parameters into a structure for ease of use
Parameters = struct('rho', rho, 'phi', phi, 'T', T, 'w', w, ...
                    'a', a, 'b', b, 'Ks', Ks, 'Kh', Kh);

% Organize boundary conditions into a structure
Boundaries = struct('G0', G0, 'S0', S0, 'F0', F0, 'P0', P0, 'd34S0', d34S0);

% Calculate Dependent Parameters
Parameters.fG = calcfG(Parameters.phi, Parameters.rho);
Parameters.fF = calcfF(Parameters.phi, Parameters.rho);
Parameters.fP = calcfP(Parameters.phi, Parameters.rho);
Parameters.kg0 = calkSW(Parameters.w); % OC reactivity

% Calculate diffusivities
D0 = 7.12 * Parameters.T + 156.5; % Sulfate diffusivity
Parameters.DS = D0 / (1 - 2 * log(Parameters.phi));
DH0 = 600 + 12.1 * Parameters.T;  % H2S Diffusivity
Parameters.DH = DH0 / (1 - 2 * log(Parameters.phi));

% Calculate non-dimensional parameters and boundaries
NonDimParameters = makeNonDimParameters(Parameters, Boundaries);
NonDimBoundaries = makeNonDimBoundaries(Parameters, Boundaries);
NonDimBoundaries.Eta0 = 0;

%% Calculate Non-Dimensional Depth
maxDepth = Depth * sqrt(Parameters.kg0 / Parameters.DS);
zetamesh = linspace(0, maxDepth, 300); % Solver grid

%% Calculate Solution
ModelSolution = ModelSolverNonDim(NonDimBoundaries, NonDimParameters, zetamesh);

%% Plot Solution
% Non-dimensional plot
makeconceptualplot(ModelSolution, 'k');
% Dimensional plot
makeconceptualplot_dim(ModelSolution, Parameters, Boundaries, 'k');

